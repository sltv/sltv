# -*- coding: utf-8 -*-
# Copyright (C) 2010 Holoscopio Tecnologia
# Author: Marcelo Jorge Vieira <metal@holoscopio.com>
# Author: Thadeu Lima de Souza Cascardo <cascardo@holoscopio.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import gobject
import pygst
pygst.require("0.10")
import gst
from core import Input, INPUT_TYPE_VIDEO
from sltv.utils import Fract

CAPABILITIES = INPUT_TYPE_VIDEO

# Ugly import trick because I messed up the name...
pyxrandr = __import__('xrandr.xrandr', globals(), locals(), [], 0)

class XRandR(Input):

    def __init__(self):
        Input.__init__(self, CAPABILITIES)
        self.video_src = gst.element_factory_make("ximagesrc", "video_src")

        # Setting format to time, to work with input-selector, since they're
        # were not working together in version 0.10.18-1 from Debian.
        # This should be fixed in ximagesrc's code and input-selector should
        # also be fixed to work with byte format.

        self.video_src.set_format(gst.FORMAT_TIME)
        self.video_src.set_property("use-damage", False)

        self.add(self.video_src)
        self.capsfilter = gst.element_factory_make("capsfilter", "capsfilter")
        self.add(self.capsfilter)

        gst.element_link_many(self.video_src, self.capsfilter)

        self.video_pad.set_target(self.capsfilter.src_pads().next())

    def config(self, dict):
        output_name = dict['output']

        screen = pyxrandr.xrandr.get_current_screen()
        output = screen.outputs[output_name]
        output_crtc = screen.get_crtc_by_xid(output.get_crtc())
        output_crtc_info = output_crtc._info.contents

        self.video_src.set_property("startx", output_crtc_info.x)
        self.video_src.set_property("starty", output_crtc_info.y)
        self.video_src.set_property("endx", output_crtc_info.x + output_crtc_info.width - 1)
        self.video_src.set_property("endy", output_crtc_info.y + output_crtc_info.height - 1)
