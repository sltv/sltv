# -*- coding: utf-8 -*-
# Copyright (C) 2010 Holoscópio Tecnologia
# Author: Marcelo Jorge Vieira <metal@holoscopio.com>
# Author: Thadeu Lima de Souza Cascardo <cascardo@holoscopio.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import gobject
import gtk
from sltv.settings import UI_DIR
from core import InputUI

import pyudev
import v4l2
import fcntl
import errno

class V4L2InputUI(InputUI):
    def __init__(self):
        InputUI.__init__(self)
        cell = gtk.CellRendererText()

        self.interface.add_from_file(UI_DIR + "/input/v4l2input.ui")
        self.box = self.interface.get_object("v4l2_box")
        self.device_combo = self.interface.get_object("v4l2_combo")
        self.format_list = self.interface.get_object("v4l2_format_list")

        self.device_combo.connect("changed", self.set_format_model)
        self.device_combo.set_model(self.get_devices_model())
        self.device_combo.pack_start(cell, True)
        self.device_combo.add_attribute(cell, 'text', 1)

        self.format_list.append_column(gtk.TreeViewColumn('Available Formats', gtk.CellRendererText(), text=2))

    def get_widget(self):
        return self.box

    def get_name(self):
        return "V4L2"

    def get_description(self):
        return "Get Video from V4L2"

    def update_config(self):
        device_name = self.config["v4l2_device"]
        width = int(self.config["width"])
        height = int(self.config["height"])
        device_found = True
        device_model = self.device_combo.get_model()
        device_model_iter = device_model.get_iter_first()
        format_model = None
        format_model_iter = None

        while device_model_iter and (device_model.get(device_model_iter, 0)[0] != device_name):
            device_model_iter = device_model.iter_next(device_model_iter)

        if not device_model:
            device_found = False
            device_model_iter = device_model.iter_first()

        if device_model_iter:
            self.device_combo.set_active_iter(device_model_iter)

            format_model = device_model.get(device_model_iter, 2)[0]
            format_model_iter = format_model.get_iter_first()
        
            if device_found:
                while format_model_iter and (format_model.get(format_model_iter, 0, 1) != (width, height)):
                    format_model_iter = format_model.iter_next(format_model_iter)

                if not format_model_iter:
                    format_model_iter = format_model.iter_first()

                if format_model_iter:
                    self.format_list.get_selection().select_iter(format_model_iter)

    def get_config(self):
        device_model = self.device_combo.get_model()
        device_model_iter = self.device_combo.get_active_iter()
        format_model, format_model_iter = self.format_list.get_selection().get_selected()

        self.config["v4l2_device"] = device_model.get(device_model_iter, 0)[0]
        self.config["width"], self.config["height"] = format_model.get(format_model_iter, 0, 1)

        return self.config

    def get_devices_model(self):
        udev_context = pyudev.Context()
        devices_model = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_STRING, gtk.ListStore)

        for dev in udev_context.list_devices(subsystem='video4linux'):
            format_model = gtk.ListStore(gobject.TYPE_LONG, gobject.TYPE_LONG, gobject.TYPE_STRING)
            dev_filename = dev.device_node
            devices_model.append([dev_filename, dev.attributes['name'], format_model])
            format_list = []
            vd = open(dev_filename, 'rw')
            fd = v4l2.v4l2_fmtdesc()
    
            fd.type = v4l2.V4L2_BUF_TYPE_VIDEO_CAPTURE

            self.iterate_format_description(vd, fd, format_list)

            for f in format_list:
                format_model.append([f[0], f[1], "%sx%s" % (f[0], f[1])])

        return devices_model

    def iterate_format_description(self, vd, fd, format_list):
        fd.index = 0

        while True:
            try:
                fs = v4l2.v4l2_frmsizeenum()

                fcntl.ioctl(vd, v4l2.VIDIOC_ENUM_FMT, fd)
                fs.pixel_format = fd.pixelformat

                self.append_format_sizes(vd, fs, fd.type, format_list)
                    
                fd.index += 1
            except IOError as ex:
                if ex.errno == errno.EINVAL:
                    break
                else:
                    raise ex

    def append_format_sizes(self, vd, fs, format_type, format_list):
        fs.index = 0

        while True:
            try:
                fs.type = format_type
                fcntl.ioctl(vd, v4l2.VIDIOC_ENUM_FRAMESIZES, fs)
                            
                if (fs.discrete.width, fs.discrete.height) not in format_list:
                    format_list.append((fs.discrete.width, fs.discrete.height))

                fs.index += 1
            except IOError as ex:
                if ex.errno == errno.EINVAL:
                    break
                else:
                    raise ex

    def set_format_model(self, combo_box):
        selected_device_iter = self.device_combo.get_active_iter()
        format_model = self.device_combo.get_model().get(selected_device_iter, 2)[0]
        self.format_list.set_model(format_model)
