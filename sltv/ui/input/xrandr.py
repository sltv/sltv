# -*- coding: utf-8 -*-
# Copyright (C) 2010 Holoscópio Tecnologia
# Author: Marcelo Jorge Vieira <metal@holoscopio.com>
# Author: Thadeu Lima de Souza Cascardo <cascardo@holoscopio.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import gobject
import gtk
from sltv.settings import UI_DIR
from core import InputUI

# TODO Port to model
pyxrandr = __import__('xrandr.xrandr', globals(), locals(), [], 0)

class XRandRUI(InputUI):
    def __init__(self):
        InputUI.__init__(self)
        self.interface.add_from_file(UI_DIR + "/input/xrandr.ui")
        self.output_list = self.interface.get_object("output_list")
        self.output_list.append_column(gtk.TreeViewColumn('Output Name', gtk.CellRendererText(), text=0))
        self.update_config()

    def get_widget(self):
        return self.output_list

    def get_name(self):
        return "XRandR"

    def get_description(self):
        return "Get Video from Desktop (XRandR)"

    def update_config(self):
        screen = pyxrandr.xrandr.get_current_screen()
        outputs_model = gtk.ListStore(gobject.TYPE_STRING)

        self.output_list.set_model(outputs_model)

        for output_name in screen.outputs:
            if screen.outputs[output_name].is_active():
                output_iter = outputs_model.append([output_name])

                if ('output' not in self.config) or (self.config['output'] == output_name):
                    self.config['output'] = output_name
                    self.output_list.get_selection().select_iter(output_iter)

    def get_config(self):
        (outputs_model, selected_iter) = self.output_list.get_selection().get_selected()

        self.config['output'] = outputs_model.get(selected_iter, 0)[0]

        return self.config
